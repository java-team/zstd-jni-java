import com.github.luben.zstd.Zstd;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class ZstdTest {
  @Test
  public static void main(String[] args) {
    final String testString = "testing for Bazel in Debian";
    final byte[] testStringToBytes = testString.getBytes();
    final int originalSize = testStringToBytes.length;
    final byte[] testBytesCompressed = Zstd.compress(testStringToBytes);
    final byte[] testBytesDecompressed = Zstd.decompress(testBytesCompressed, originalSize);
    Assert.assertEquals(testString, new String(testBytesDecompressed));
  }
}
